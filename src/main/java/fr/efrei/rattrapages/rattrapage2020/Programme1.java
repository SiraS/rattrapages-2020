package fr.efrei.rattrapages.rattrapage2020;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.ArrayList;
import java.util.List;

public class Programme1 {
    public static void main(String[] args) {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Developper("Project A"));
        employees.add(new Developper("Project B"));
        employees.add(new Administrator());
        employees.add(new Auditor());

        for(Employee employee: employees) {
            System.out.println(employee.getClass().toString() + " :");
            testReadAccess(employee, "Project A");
            testWriteAccess(employee, "Project A");
            testReadAccess(employee, "Project B");
            testWriteAccess(employee, "Project B");
            System.out.println("");
        }

        /* Résultat attendu :
                class fr.efrei.rattrapages.rattrapage2020.Programme1$Developper :
                can read code of Project A
                can write code into Project A
                cannot read code of Project B
                cannot write code into Project B

                class fr.efrei.rattrapages.rattrapage2020.Programme1$Developper :
                cannot read code of Project A
                cannot write code into Project A
                can read code of Project B
                can write code into Project B

                class fr.efrei.rattrapages.rattrapage2020.Programme1$Administrator :
                can read code of Project A
                can write code into Project A
                can read code of Project B
                can write code into Project B

                class fr.efrei.rattrapages.rattrapage2020.Programme1$Auditor :
                can read code of Project A
                cannot write code into Project A
                can read code of Project B
                cannot write code into Project B
         */
    }

    private static void testReadAccess(Employee employee, String project) {
        System.out.println((employee.canAccessCode(false, project) ? "can" : "cannot") + " read code of " + project);
    }

    private static void testWriteAccess(Employee employee, String project) {
        System.out.println((employee.canAccessCode(true, project) ? "can" : "cannot") + " write code into " + project);
    }

    // TODO : CREER DES CLASSES ET AUTRES ELEMENTS CI-DESSOUS
    public static class Employee {
        protected String project;

        public Employee() {

        }

        protected boolean canAccessCode(Boolean isWriteMode, String project) {
            if (isWriteMode) {
               return true;
            } else {
                return false;
            }
        }
    }

    public static class Developper extends Employee {
        public Developper(String project) {
            this.project = project;
        }

        protected boolean canAccessCode(Boolean isWriteMode, String project) {
            return this.project == project;
        }
    }

    public static class Administrator extends Employee {
        public Administrator() {

        }

        protected boolean canAccessCode(Boolean condition, String project) {
            return true;
        }
    }

    public static class Auditor extends  Employee {
        public Auditor() {

        }

        protected boolean canAccessCode(Boolean isWriteMode, String project) {
            if (isWriteMode) {
                return false;
            } else {
                return true;
            }
        }
    }

}

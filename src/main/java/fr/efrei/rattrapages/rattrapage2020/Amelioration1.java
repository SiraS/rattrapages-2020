package fr.efrei.rattrapages.rattrapage2020;

import java.util.ArrayList;
import java.util.List;

public class Amelioration1 {
    /*
        La méthode suivante a pour but d’identifier un groupe de note considérées comme représentatives
        de l’ensemble, et ce via un algorithme relativement basique.

        Elle reçoit une listetrouverNotesRepresentatives de notes, allant de 0 à 20, et les divise en groupe de tailles égale.

        Le second paramètre qualifie le nombre de groupes.
        S’il vaut 2, il y aura deux groupes, le premier regroupant les notes allant de 0 à 10,
        le second de 10 à 20.
        S’il vaut 8, il y aura 8 groupes, le premier allant de 0 à (20/8=2,5), le second de 2,5 à 5,
        le troisième de 5 à 7,5… et le 8ème regroupera les notes allant de 17,5 à 20.

        Une fois regroupée, la méthode trouve le groupe comportant le plus de notes, et retourne ces
        dernières.
        L’idée est que si le 5ème groupe a le plus de notes, alors on peut dire que de nombreux élèves
        ont des notes similaires.
     */
    private static List<List<Double>> prepareList(List<Double> l1) {
        List<List<Double>> l2 = new ArrayList<List<Double>>();

        for (int m = 0; m < i; m++) {
            l2.add(new ArrayList<Double>());
        }

        return l2;
    }

    private static void groupMarkByList(List<Double> marks, List<List<Double>> groupedMarks, int numOfMarkList) {
        Double j = 20.0 / numOfMarkList;

        for (Double n: marks) {
            for (int m = 0; m < i; m++) {
                Double k = (m + 1) * j;
                if (n <= k) {
                    groupedMarks.get(m).add(n);
                    break;
                }
            }
        }
    }

    private static List<Double> findHighestList(List<List<Double>> groupedMarks) {
        int o = groupedMarks.get(0).size();
        int p = 0;
        for (int m = 0; m < i; m++) {
            if (groupedMarks.get(m).size() > o) {
                p = m;
                o = groupedMarks.get(m).size();
            }
        }

        return groupedMarks.get(p);
    }

    public static List<Double> trouverNotesRepresentatives(List<Double> marks, int numOfMarkList) {
        List<List<Double>> groupedMarks = prepareList(marks);
        groupMarkByList(marks, groupedMarks, numOfMarkList);

        return findHighestList(groupedMarks, numOfMarkList);
    }
}

package fr.efrei.rattrapages.rattrapage2020;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Programme2 {
    public static void main(String[] args) {
        List<Vote> votes = new ArrayList<>();
        votes.add(new Vote("John Doe", 14, VoteOption.PROPOSING));
        votes.add(new Vote("Jane Doe", 32, VoteOption.AGAINST));
        votes.add(new Vote("John Smith", 22, VoteOption.FAVORABLE));
        votes.add(new Vote("Jack Doe", 7, VoteOption.ABSTAINING));
        votes.add(new Vote("Jack Smith", 6, VoteOption.BLANK));
        votes.add(new Vote("Jane Smith", 19, VoteOption.FAVORABLE));

        int favorableWeight = getFavorableWeight(votes);
        int totalWeight = getTotalWeight(votes);
        System.out.println("Vote result : " + (100.0 * favorableWeight / totalWeight) + "% in favor (" + favorableWeight + " / " + totalWeight + ")");

        for(String detailedLine: getVoteDetailsOrderedByWeight(votes)) {
            System.out.println(detailedLine);
        }

        /* Résultat attendu :
                Vote result : 55.0% in favor (55 / 100)
                Jane Doe (32) : AGAINST
                John Smith (22) : FAVORABLE
                Jane Smith (19) : FAVORABLE
                John Doe (14) : PROPOSING
                Jack Doe (7) : ABSTAINING
                Jack Smith (6) : BLANK
         */
    }

    private static int getFavorableWeight(List<Vote> votes) {
        return 0;
    }

    private static int getTotalWeight(List<Vote> votes) {
        Integer totalWeight = votes.stream().mapToInt(vote -> vote.weight).sum();

        return totalWeight;
    }

    private static List<String> getVoteDetailsOrderedByWeight(List<Vote> votes) {
        List<String> orderedVotes = votes.stream()
                .sorted(Comparator.comparingInt(vote -> vote.weight))
                .map(vote -> vote.voter)
                .collect(Collectors.toList());

        return orderedVotes;
    }

    public static class Vote {
        private final String voter;
        private final int weight;
        private final VoteOption chosenOption;

        public Vote(String voter, int weight, VoteOption chosenOption) {

            this.voter = voter;
            this.weight = weight;
            this.chosenOption = chosenOption;
        }

        public String getVoter() {
            return voter;
        }

        public int getWeight() {
            return weight;
        }

        public VoteOption getChosenOption() {
            return chosenOption;
        }
    }

    public enum VoteOption {
        PROPOSING(true), // FR: A soumis l'objet du vote
        FAVORABLE(true), // FR: Favorable
        ABSTAINING(false), // FR: Abstention
        BLANK(false), // FR: Vote blanc
        AGAINST(false) // FR: Vote contre
        ;

        private final boolean isInFavor;

        VoteOption(boolean isInFavor) {
            this.isInFavor = isInFavor;
        }

        public boolean isInFavor() {
            return isInFavor;
        }
    }
}
